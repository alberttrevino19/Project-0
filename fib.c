#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

const int MAX = 13;

static void doFib(int n, int doPrint);


/*
 * unix_error - unix-style error routine.
 */
inline static void unix_error(char *msg)
{
    fprintf(stdout, "%s: %s\n", msg, strerror(errno));
    exit(1);
}


int main(int argc, char **argv)
{
    int arg;
    int print=1;

    if(argc != 2){
        fprintf(stderr, "Usage: fib <num>\n");
        exit(-1);
    }

    arg = atoi(argv[1]);
    if(arg < 0 || arg > MAX){
        fprintf(stderr, "number must be between 0 and %d\n", MAX);
        exit(-1);
    }

    doFib(arg, print);

    return 0;
}

//Fork wrapper from text, Bryant, O'Hallaron, pg. 738
pid_t Fork(void){
	pid_t pid;

	if((pid = fork()) < 0){
		unix_error("Fork Error");
	}

	return pid;
}

// helper for doFib
int getChildVal(int childN)
{
	// McCray and Albert are driving
	int status;
	if(childN == 1){
		return 1;
	}
	else if(childN > 1){ //child
		pid_t pid = Fork();
		if(pid == 0){
			doFib(childN, 0);
		}
		waitpid(pid, &status, 0);
		//Get return value from child
		if(WIFEXITED(status)){
			return WEXITSTATUS(status);
		}
		else{
			unix_error("Child terminated abnormally");
		}
	}
	return 0;
}

/* 
 * Recursively compute the specified number. If print is
 * true, print it. Otherwise, provide it to my parent process.
 *
 * NOTE: The solution must be recursive and it must fork
 * a new child for each call. Each process should call
 * doFib() exactly once.
 */
static void doFib(int n, int doPrint)
{
  //Albert and McCray are driving
	int sum = 0;

	//child 1
	sum += getChildVal(n-1);

	//child 2
	sum += getChildVal(n-2);


	if(n == 1){ //this should only ever be true if the initial value is 1;
		sum+=1;
	}
	if(doPrint == 1){ //Only true if it is the parent
		printf("%d\n", sum);
	}
	else{
		exit(sum);
	}

}

