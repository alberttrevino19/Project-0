#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include "util.h"


/*
 * First, print out the process ID of this process.
 *
 * Then, set up the signal handler so that ^C causes
 * the program to print "Nice try.\n" and continue looping.
 *
 * Finally, loop forever, printing "Still here\n" once every
 * second.
 */

//handler for SIGINT
//Albert and McCray is driving
void sig_handler(int sig){
	if (sig == SIGINT)
	{
		//found from project 0 page
		ssize_t bytes; 
		const int STDOUT = 1; 
		bytes = write(STDOUT, "Nice try.\n", 10); 
		if(bytes != 10) 
	   		exit(-999);
	}
	if (sig == SIGUSR1)
	{
		//found from project 0 page
		ssize_t bytes; 
		const int STDOUT = 1; 
		bytes = write(STDOUT, "exiting\n", 10); 
		if(bytes != 10) 
	   		exit(-999);
	   	exit(1);
	}
	
}

int main(int argc, char **argv)
{
	// Mccray driving
	// print PID
	pid_t pid = getpid();
	printf("%d\n", pid);

	//one second struct for nano sleep
	struct timespec* req = NULL; 
	req = malloc(sizeof(struct timespec));
	req -> tv_sec = 1;

	//struct that keeps track of how many seconds are remaining in-between prints
	struct timespec* remaining = NULL; 
	remaining = malloc(sizeof(struct timespec));

	//variable that keeps track of when our loop is interupted
	int isInter = 0;

	// set up signal handler and loop
	while (1)
	{
		// Albert is driving
		// print 'still here' message every second
		//Is our loop interrupted? if not, print and restart
		if(isInter == 0){
			printf("Still here\n");
			//nanosleep for one second
			isInter = nanosleep(req, remaining);
		}
		//Install the signal handler
		Signal(SIGINT, sig_handler);
		Signal(SIGUSR1, sig_handler);
		//Is our loop interupted
		if(isInter == -1){
			//if so, continue the timer with the remaining seconds
			isInter = nanosleep(remaining, remaining);
		}
	}


  return 0;
}


