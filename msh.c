/* 
 * msh - A mini shell program with job control
 * 
 * Albert C. Trevino - Atrevi21
 * McCray Robertson - mccray
 * 2/14/20
 * Mini shell
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include "util.h"
#include "jobs.h"

/*Constants*/

/* Global variables */
int verbose = 0;            /* if true, print additional output */

extern char **environ;      /* defined in libc */
static char prompt[] = "msh> ";    /* command line prompt (DO NOT CHANGE) */
static struct job_t jobs[MAXJOBS]; /* The job list */

/* End global variables */


/* Function prototypes */

/* Here are the functions that you will implement */
void eval(char *cmdline);
int builtin_cmd(char **argv);
void do_bgfg(char **argv);
void waitfg(pid_t pid);

void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);

/* Here are helper routines that we've provided for you */
void usage(void);
void sigquit_handler(int sig);

int Kill(pid_t pid, int sig);
void job_reaper(struct job_t* job);
void sigPrint(char* buffer, int bufferSize);



/*
 * main - The shell's main routine 
 */
int main(int argc, char **argv) 
{
    char c;
    char cmdline[MAXLINE];
    int emit_prompt = 1; /* emit prompt (default) */

    /* Redirect stderr to stdout (so that driver will get all output
     * on the pipe connected to stdout) */
    dup2(1, 2);

    /* Parse the command line */
    while ((c = getopt(argc, argv, "hvp")) != EOF) {
        switch (c) {
        case 'h':             /* print help message */
            usage();
	    break;
        case 'v':             /* emit additional diagnostic info */
            verbose = 1;
	    break;
        case 'p':             /* don't print a prompt */
            emit_prompt = 0;  /* handy for automatic testing */
	    break;
	default:
            usage();
	}
    }

    /* Install the signal handlers */

    /* These are the ones you will need to implement */
    Signal(SIGINT,  sigint_handler);   /* ctrl-c */
    Signal(SIGTSTP, sigtstp_handler);  /* ctrl-z */
    Signal(SIGCHLD, sigchld_handler);  /* Terminated or stopped child */

    /* This one provides a clean way to kill the shell */
    Signal(SIGQUIT, sigquit_handler); 

    /* Initialize the job list */
    initjobs(jobs);

    /* Execute the shell's read/eval loop */
    while (1) {

	/* Read command line */
	if (emit_prompt) {
	    printf("%s", prompt);
	    fflush(stdout);
	}
	if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
	    app_error("fgets error");
	if (feof(stdin)) { /* End of file (ctrl-d) */
	    fflush(stdout);
	    exit(0);
	}

	/* Evaluate the command line */
	eval(cmdline);
	fflush(stdout);
	fflush(stdout);
    } 

    exit(0); /* control never reaches here */
}

//Fork wrapper from text, Bryant, O'Hallaron, pg. 738
pid_t Fork(void){
    pid_t pid;

    if((pid = fork()) < 0){
        unix_error("Fork Error");
    }

    return pid;
}
  
/* 
 * eval - Evaluate the command line that the user has just typed in
 * 
 * If the user has requested a built-in command (quit, jobs, bg or fg)
 * then execute it immediately. Otherwise, fork a child process and
 * run the job in the context of the child. If the job is running in
 * the foreground, wait for it to terminate and then return.  Note:
 * each child process must have a unique process group ID so that our
 * background children don't receive SIGINT (SIGTSTP) from the kernel
 * when we type ctrl-c (ctrl-z) at the keyboard.  
*/

int test = 0;
void eval(char *cmdline) 
{
    /*McCray is driving
    parse commmand line*/
    int state;
    char* argv[MAXLINE];
    int isBG = parseline(cmdline, argv);

     // is child running in bg or fg?
    state = isBG ? BG : FG;

    if (argv[0] != NULL && builtin_cmd(argv) == 0)
    {
        // fork child and run as job
        //Albert and McCray are driving

        // create new sigset
        sigset_t blockedsig;
        sigemptyset(&blockedsig);
        // add SIGCHLD to set
        sigaddset(&blockedsig, SIGCHLD);

        //block SIGCHLD
        if(sigprocmask(SIG_BLOCK, &blockedsig, NULL) == -1){
            unix_error("sigprocmask block error");
        }

        pid_t pid = Fork();

        if (pid == 0) //if child
        {
            setpgid(0, 0); //give the child a new pgid

            //unblock SIGCHLD signal
            if(sigprocmask(SIG_UNBLOCK, &blockedsig, NULL) == -1){ 
                unix_error("sigprocmask unblock error");
            }
            //exec job
            
            execv(argv[0], argv);
            
            // exec error
            printf("%s: Command not found\n", argv[0]);
            exit(1);

        }
        // wait on child if running in fg, then delete job
        // otherwise, don't wait and let handler reap
        else if (state == FG)
        {
            if (addjob(jobs, pid, state, cmdline) == 0) // add job to list
                app_error("addjob error");

            //unblock SIGCHLD signal
            if(sigprocmask(SIG_UNBLOCK, &blockedsig, NULL) == -1){ 
                unix_error("sigprocmask unblock error");
            }
            waitfg(pid);
        }
        else if(state == BG)
        {
            if (addjob(jobs, pid, state, cmdline) == 0) // add job to list
                app_error("addjob error");

 			//unblock SIGCHLD signal
            if(sigprocmask(SIG_UNBLOCK, &blockedsig, NULL) == -1){
                unix_error("sigprocmask unblock error");
            }

            struct job_t* newJob = getjobpid(jobs, pid);
            printf("[%d] (%d) %s", newJob->jid, newJob->pid, newJob->cmdline);
        }
        
    }

    return;
}


/* 
 * builtin_cmd - If the user has typed a built-in command then execute
 *    it immediately.  
 * Return 1 if a builtin command was executed; return 0
 * if the argument passed in is *not* a builtin command.
 */
int builtin_cmd(char **argv) 
{
    // McCray is driving
    // check for empty comand line

    char* cmd = argv[0]; // command given from cmd line

    char quitCmd[] = "quit";
    char jobsCmd[] = "jobs";
    char bgCmd[] = "bg";
    char fgCmd[] = "fg";


    // check if user types in built-in command
    if (strcmp(cmd, quitCmd) == 0)
    {
        // command given is quit
        exit(1);

    }
    else if (strcmp(cmd, jobsCmd) == 0)
    {
        // command given is jobs
        listjobs(jobs);

        return 1;
    }
    else if (strcmp(cmd, bgCmd) == 0)
    {
        do_bgfg(argv);
        return 1;
    }
    else if (strcmp(cmd, fgCmd) == 0)
    {
        do_bgfg(argv);
        return 1;
    }
    return 0;     /* not a builtin command */
}

/*error wrapper for kill
pid is process to send signal to
sig is the signal to be sent
returns result of system call to kill*/
int Kill(pid_t pid, int sig)
{
    //McCray driving
    int result = kill(pid, sig);

    if(result < 0){
        unix_error("Kill Error");
    }

    return result;
}


/* 
 * do_bgfg - Execute the builtin bg and fg commands
 */
void do_bgfg(char **argv) 
{
    
    //Albert and mcCray is driving
	// error check for correct input
	if (argv[1] == NULL)
	{
		printf("%s command requires PID or %%jobid argument\n", argv[0]);
		return;
	}

    struct job_t* thisJob;
    int status, waitResult;

    // block child signals until restart complete
    // create new sigset
    sigset_t mask;
    sigemptyset(&mask);
    // add SIGCHLD to set
    sigaddset(&mask, SIGCHLD);
    if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1)
        unix_error("sigprocmask block error");

    // get job requested
    if(argv[1][0] == '%'){ //are we given a jid
        int thisJID = atoi(argv[1] + 1); //get rid of %
        thisJob = getjobjid(jobs, thisJID);
        // check if jid is valid
        if (thisJob == NULL)
        {
        	printf("%s: No such job\n", argv[1]);
        	return;
        }
    }
    else if (argv[1][0] >= '0' && argv[1][0] <= '9'){
        pid_t thisPID = atoi(argv[1]); //if we're given the pid
        thisJob = getjobpid(jobs, thisPID);
        // check if pid is valid
        if (thisJob == NULL)
        {
        	printf("(%s): No such process\n", argv[1]);
        	return;
        }
    }
    else
    {
    	printf("%s: argument must be a PID or %%jobid\n", argv[0]);
    	return;
    }

    // check if job needs to be stopped
    waitResult = waitpid(thisJob->pid, &status, WNOHANG | WUNTRACED);
    if (waitResult == -1)
        unix_error("waitpid error");
    else if (waitResult == 0)
    {
        // only send stop signal to process still running
        Kill(-(thisJob->pid), SIGTSTP);
    }

    // now have job in need of restarting
    // are we restarting in bg or fg?
    char bgCmd[] = "bg";
    if (strcmp(argv[0], bgCmd) == 0)
    {
        // want to run in background
        // change job state
        thisJob->state = BG;
        // print job is continuing in bg
        printf("[%d] (%d) %s", thisJob->jid, thisJob->pid, thisJob->cmdline);
        // continue job
        if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
            unix_error("sigprocmask block error");
        Kill(-(thisJob->pid), SIGCONT);//send SIGCONT signal
    }
    else
    {
        // want to run in foreground
        // change job state
        thisJob->state = FG;
        // continue job
        if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
            unix_error("sigprocmask block error");
        Kill(-(thisJob->pid), SIGCONT); //send SIGCONT signal
        // wait on fg job
        waitfg(thisJob->pid);
    }

    return;
}


/*  wrapper function to acively change the state of a job. If a job
    has terminated, immeadiately removes it from the job list (job is reaped),
    if the job has stopped, just update its state. 
    Parameter job is the job that is being zombie checked
    Does not return anything
*/
void job_reaper(struct job_t* job){
    // McCray and Albert driving
    int status, pid, jid, sigNum;
    pid_t waitReturn;
    pid = job->pid;
    jid = job->jid;

    waitReturn = waitpid(pid, &status, WNOHANG | WUNTRACED);
    if (waitReturn > 0)
    {
     // update job state if changed
	    if (WIFEXITED(status) || WIFSIGNALED(status))
	    {
	        // check if job was terminated due to signal
	        if (WIFSIGNALED(status))
	        {
	            // if terminated via a signal, print that info
	            sigNum = WTERMSIG(status);
	            char buf[60];
	            if(sprintf(buf, "Job [%d] (%d) terminated by signal %d",
	                jid,
	                pid,
	                sigNum) < 0)
	            {
	            	unix_error("sprintf error");
	            }

	            if(puts(buf) < 0){
	            	unix_error("puts error");
	            }
	            
	        }
	        //erase the job if needed
	        deletejob(jobs, pid);
	    }
	    else if(WIFSTOPPED(status))
	    {
	        // print that job stopped and what signal stopped it
	        // create buffer and print it
	        sigNum = WSTOPSIG(status);
	        char buf[60];
	        if(sprintf(buf, "Job [%d] (%d) stopped by signal %d",
	            jid,
	            pid,
	            sigNum) < 0)
	        {
	            unix_error("sprintf error");
	        }

	        if(puts(buf) < 0){
	        	unix_error("puts error");
	        }
	        // update job state
	        job -> state = ST;
	    }
	}
	else if (waitReturn == -1)
	{
		// waitpid errored
		unix_error("waitpid error");
	}
}

/*
    Takes a string and writes it out. Used for signal handling
    (when printf cannot be used).
    No return
*/
void sigPrint(char* buffer, int bufferSize)
{
    ssize_t bytes; 
    const int STDOUT = 1; 
    bytes = write(STDOUT, buffer, bufferSize); 
    if(bytes != bufferSize) 
       exit(-999);
    exit(1);
}

/* 
 * waitfg - Block until process pid is no longer the foreground process
 */
void waitfg(pid_t pid)
{
    // McCray and Albert are driving

    //check if still in the foreground

    struct job_t* fgJob = getjobpid(jobs, pid); // foreground job
    
    while (fgJob->state == FG)
    {
        //suspend the foreground job
        // create new sigset
        sigset_t mask;
        sigemptyset(&mask);
        // add SIGCHLD to set
        sigaddset(&mask, SIGTSTP);
        sigaddset(&mask, SIGINT);

        if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1)
            unix_error("sigprocmask block error");

        // handle terminated jobs while waiting (SIGCHLD_handler)
        Kill(getpid(), SIGCHLD);

        // unblock fg
        if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
            unix_error("sigprocmask unblock error");

    }
    return;
}

/*****************
 * Signal handlers
 *****************/

/* 
 * sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
 *     a child job terminates (becomes a zombie), or stops because it
 *     received a SIGSTOP or SIGTSTP signal. The handler reaps all
 *     available zombie children, but doesn't wait for any other
 *     currently running children to terminate.  
 */
void sigchld_handler(int sig) 
{
    //McCray and Albert are driving
    //block sigchld signals until finished
    // create new sigset
    sigset_t mask;
    sigemptyset(&mask);
    // add SIGCHLD to set
    sigaddset(&mask, SIGCHLD);

    if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1)
        unix_error("sigprocmask block error");
    //check for zombies
    for(int i = 0; i < MAXJOBS; i++){
        struct job_t* currentJob = &jobs[i];

        if(currentJob != NULL && currentJob -> state != UNDEF){
            job_reaper(currentJob);
        }
    }

    //unblock sigchld signals
    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
            unix_error("sigprocmask unblock error");
    return;
}

/* 
 * sigint_handler - The kernel sends a SIGINT to the shell whenver the
 *    user types ctrl-c at the keyboard.  Catch it and send it along
 *    to the foreground job.  
 */
void sigint_handler(int sig) 
{
    // McCray is driving
    pid_t fgPID = fgpid(jobs); // get pid of foreground job
    Kill(-fgPID, sig); // send signal to PID
    return;
}

/*
 * sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
 *     the user types ctrl-z at the keyboard. Catch it and suspend the
 *     foreground job by sending it a SIGTSTP.  
 */
void sigtstp_handler(int sig) 
{
    // McCray is driving
    pid_t fgPID = fgpid(jobs); // get pid of foreground job
    Kill(-fgPID, sig); // send signal to PID
    return;
}

/*********************
 * End signal handlers
 *********************/



/***********************
 * Other helper routines
 ***********************/

/*
 * usage - print a help message
 */
void usage(void) 
{
    printf("Usage: shell [-hvp]\n");
    printf("   -h   print this message\n");
    printf("   -v   print additional diagnostic information\n");
    printf("   -p   do not emit a command prompt\n");
    exit(1);
}

/*
 * sigquit_handler - The driver program can gracefully terminate the
 *    child shell by sending it a SIGQUIT signal.
 */
void sigquit_handler(int sig) 
{
    ssize_t bytes;
    const int STDOUT = 1;
    bytes = write(STDOUT, "Terminating after receipt of SIGQUIT signal\n", 45);
    if(bytes != 45)
       exit(-999);
    exit(1);
}



