#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include "util.h"

// error wrapper for kill
int Kill(pid_t pid, int sig)
{
	//McCray driving
	int result = kill(pid, sig);

	if(result < 0){
		unix_error("Kill Error");
	}

	return result;
}

int main(int argc, char **argv)
{
	//McCray is driving
	// error check arguments
	if (argv[1] == NULL)
	{
		app_error("mykill rquires PID");
	}
	pid_t pid = atoi(argv[1]);
	Kill(pid, SIGUSR1);

	return 0;
}
